# Bài tập phỏng vấn DSG Research Engineer

Yêu cầu xây dựng mô hình phân loại ảnh

## 1.Dữ liệu
	- Download tập ảnh tại thư mục './data'
		- tập './data/train': dùng để huấn luyện mô hình
		- tập './data/validation': dùng để đánh giá độ chính xác của mô hình
		- tập './data/test': dùng kể đưa ra giá trị delta của ảnh
	- Tên file ảnh có cấu trúc như sau 'Mr_0.1_4_4.png'. Giá trị số cuối cùng là giá trị delta của ảnh cần phân vào 4 lớp như sau:
		- Giá trị từ 1 -> 45: thuộc lớp 1
		- Giá trị từ 46 -> 90: thuộc lớp 2
		- Giá trị từ 91 -> 135: thuộc lớp 3
		- Giá trị từ 135 -> 180: thuộc lớp 4

## 2.Thống kê phân bố trên các lớp dữ liệu
	- Viết 1 python script thống kê số lượng các samples trong mỗi lớp trong tập dữ liệu './train'

## 3.Xây dựng mô hình
	- Viết chương trình huấn luyện sử dụng 2 thuật toán là Softmax và Convolutional Neural Network để xây dựng mô hình phân lớp cho ảnh trên tập dữ liệu huấn luyện `./data/train`. 
	- Sau đó báo cáo kết quả trên tập dữ liệu validation ('./data/validation'). Ở đây, chúng ta sử dụng độ đo là độ chính xác (accuracy), so sánh kết quả của 2 thuật toán.

## 4.Đưa ra giá trị delta với từng ảnh
	- Với các ảnh trong thư mục './data/test', sử dụng mô hình đã học ở bên trên để đưa ra giá trị delta trên từng ảnh và lưu vào file csv với cấu trúc như sau: 
		- Tên ảnh   giá trị delta
